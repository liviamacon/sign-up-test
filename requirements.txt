botcity-framework-core>=0.2.4
botcity-framework-web>=0.1.6

botcity-maestro-sdk>=0.1.2

setuptools~=57.4.0
requests~=2.26.0
DateTime~=4.3
bs4~=0.0.1
beautifulsoup4~=4.9.3