from botcity.web import WebBot, Browser, BROWSER_CONFIGS
from botcity.maestro import *
from imap_tools import MailBox, AND
from bs4 import BeautifulSoup
from .credential import Credential
import requests
import datetime
import re

"""

    Step 1 - open browser form sign up account
    Step 2 - fill form
    Step 3 - search and read email to activate account
    Step 4 - extract link to activate account and open it
    step 5 - Open BotMaestro login, fill with data login extracted of email and log in
    step 6 - Check popup "Get Started"
    
"""


class TestSignUp(WebBot):

    def __init__(self):
        super().__init__(self)
        self._driver_path = "./chromedriver.exe"
        self._headless = False

    def open_brws(self, url):
        self.wait(2000)
        self.url = url
        res = requests.get(self.url)
        try:
            # if res.status_code == 200:
            if res.status_code >= 0 and res.status_code <= 299:
                print("The server is available, let's the browse.")
                self.browse(self.url)
                self.maximize_window()
        except requests.exceptions.HTTPError as err:
            print(res.status_code)

            raise SystemExit(err)

    def elements_for_forms(self):  # retorn data form in an list

        self.data = [Credential().email, Credential().name, Credential().surname,
                     Credential().workspace, Credential().login,
                     Credential().password_signup, Credential().confirm_pass]

        return self.data


    def fill_forms(self):
        self.elements_for_forms()  # retorn list of element for form

        print("Filling the form")
        self.wait(5000)
        self.scroll_down(20)
        self.tab()
        self.tab()

        # fill form
        for data in self.data:
            # if workspace, necessary to use backspace
            if data == Credential().workspace:
                self.control_a(wait=2000)
                self.backspace()
                self.paste(str(data))
                self.tab()
                self.tab()
            else:
                self.paste(str(data))
                self.tab()

        # click and choice country
        self.wait(3000)
        if not self.find( "country_new", matching=0.9, waiting_time=10000):
            self.not_found("country_new")
            return False
        self.click_relative(16, 28)
        self.wait(1000)
        self.type_up()
        self.enter()

        # Click to agree for BotCity terms
        self.wait(2000)
        if not self.find( "agree_now", matching=0.9, waiting_time=10000):
            self.not_found("agree_now")
            return False
        self.click_relative(-18, -1)

        # Click to sign up
        self.scroll_down(10)
        self.wait(2000)
        if not self.find( "new_sign_up", matching=0.9, waiting_time=10000):
            self.not_found("new_sign_up")
            return False
        self.click()
        self.click()
        self.wait(8000)
        self.stop_browser()

    def delete_account(self):
        # Delete previously created account
        self.url_reset_account = "https://developers.botcity.dev/app/api/test/reset"
        self.req = requests.post(self.url_reset_account)
        print(self.req.text)
        self.wait(2000)

    def not_found(self, label):
        self.maestro = BotMaestroSDK()
        self.maestro.login("https://botcity.botcity.dev/", "botcity_test", "BOT_3T9HAGCN0ZWXGU0DOHYD")
        self.maestro.new_log_entry(activity_label="sign_up_test",
                                   values={"timestamp": datetime.datetime.now().strftime("%d/%m/%Y-%H:%M"),
                                           "conclusion": "FAIL", "note": f"Element not found: {label}"})

        print(f"Element not found: {label}")

    def load_images(self):
        self.add_image("click_relative_country", self.get_resource_abspath("click_relative_country.png"))
        self.add_image("click_agree_relative", self.get_resource_abspath("click_agree_relative.png"))
        self.add_image("click_sign_up", self.get_resource_abspath("click_sign_up.png"))


class FetchEmail(TestSignUp):

    def __init__(self):
        self.email = Credential().email
        self.password_mail = Credential().password_

    def login(self):
        self.wait(8000)
        self.mailbox = MailBox('imap.gmail.com').login(self.email, self.password_mail)

    def confirm_email(self):
        self.body_email = [msg.html for msg in self.mailbox.fetch(AND(subject='Activate your BotCity account!'))]
        print(f"Fetching email 'Activate your account'")

        return self.body_email

    def collect_data_login(self):
        super().__init__()

        # Soup object email activate account
        self.soup = BeautifulSoup(str(self.body_email), 'html.parser')

        # turn in text the email body
        self.body_text = self.soup.find("p").get_text().replace("\\r\\n", "").strip()
        print(f"The email body is: {self.body_text}")

        # index user data
        self.index_user = re.search("user:", self.body_text)

        # index workspace data
        self.index_workspace = re.search("workspace:", self.body_text)

        # index In
        self.index_in = re.search("In", self.body_text)

        # Index link data
        self.index_link = re.search("link:", self.body_text)

        # Collect login data
        self.link_text = self.body_text[self.index_link.end():].strip()
        self.user_text = self.body_text[self.index_user.end():self.index_workspace.start()]
        self.workspace_text = self.body_text[self.index_workspace.end():self.index_in.start()].strip()

        print(f"This is user: {self.user_text}")
        print(f"This is workspace: {self.workspace_text}")
        print(f"This is link: {self.link_text}")
        print(f"This is password: {Credential().password_signup}")

        # Delete email found
        self.delete_email = self.mailbox.delete(
            [msg.uid for msg in self.mailbox.fetch(AND(subject='Activate your BotCity account!'))])
        print("Email 'Activate your account' deleted")

        return self.user_text, self.workspace_text, self.link_text

    def activate_link(self):
        super().__init__()

        # Opening link for activate account
        self.open_brws(str(self.link_text))

        self.scroll_down(10)
        self.wait(4000)
        if not self.find_text("click_open_link", threshold=230, waiting_time=10000):
            self.not_found("click_open_link")
            return False
        self.click()

        # Fill login
        print("Now, let's first login to BotMaestro")
        self.passw = Credential().password_signup
        self.tab()
        # test
        # # self.login_test_error = ("login error test")
        # # self.paste(self.login_test_error)
        self.paste(self.user_text)
        self.tab()
        self.paste(str(self.passw))
        self.tab()
        self.paste(str(self.workspace_text))
        self.tab()
        self.enter()
        self.wait(2000)

    def result_login(self):

        # Try find login failed, if true use assert to compare the data login input with data login collect
        try:
            if self.find( "falha_login", matching=0.97, waiting_time=10000):
                assert self.user_text == Credential().login \
                       and self.workspace_text == Credential().workspace \
                       and self.passw == Credential().password_signup

        except AssertionError:  # If login failed send alerts and error in botmaestro
            return self.login_failed == "Failed"


    def send_email(self, subject, body):
        self.maestro = BotMaestroSDK()
        self.maestro.login("https://botcity.botcity.dev/", "botcity_test", "BOT_3T9HAGCN0ZWXGU0DOHYD")

        self.email = ["livia.macon@botcity.com.br", "livia.s.macon@gmail.com"]
        self.user = ["botcity_test"]
        self.subject = subject
        self.body = body

        for mail in self.email:
            self.maestro.message(self.email, self.user, self.subject, self.body, MessageType.TEXT)

     # Temp
    def send_email_dev(self, subject, body):
        self.maestro = BotMaestroSDK()
        self.maestro.login("https://botcity.botcity.dev/", "botcity_test", "BOT_3T9HAGCN0ZWXGU0DOHYD")

        self.email = ["gabriel@botcity.dev", "livia.macon@botcity.com.br" ]
        self.user = ["botcity_test"]
        self.subject = subject
        self.body = body

        for mail in self.email:
            self.maestro.message(self.email, self.user, self.subject, self.body, MessageType.TEXT)



    def not_found(self, label):
        self.maestro = BotMaestroSDK()
        self.maestro.login("https://botcity.botcity.dev/", "botcity_test", "BOT_3T9HAGCN0ZWXGU0DOHYD")
        self.maestro.new_log_entry(activity_label="sign_up_test",
                                   values={"timestamp": datetime.datetime.now().strftime("%d/%m/%Y-%H:%M"),
                                           "conclusion": "FAIL", "note": f"Element not found: {label}"})

        print(f"Element not found: {label}")

    def load_images(self):
        self.add_image("click_open_link", self.get_resource_abspath("click_open_link.png"))
        self.add_image("falha_login", self.get_resource_abspath("falha_login.png"))










